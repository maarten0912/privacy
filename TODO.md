# What do we need to do to conform to the (new) Privacy Regulation?

- Add option of using birthday on website
- Add birthday to narrowcasting if it is specifically brallendebullebak
- Fix external apps (API) permissions on the website
- Ask for permission when taking pictures at Kick-IT camp
